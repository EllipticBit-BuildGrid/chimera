import std.stdio;
import std.string;

import chimera.build;
import chimera.restore;
import chimera.help;

int main(string[] args)
{
	string cmd = string.init;
	if (args.length < 1) {
		cmd = args[1].toLower();
	} else {
		cmd = "build";
	}

	if (cmd == "build") {
		build(args[2..$]);
	}
	else if (cmd == "init") {

	}
	else if (cmd == "restore") {
		restore(args[2..$]);
	}
	else if (cmd == "help") {
		writeHelp();
	}
	else if (cmd == "version") {
		writeVersion();
	}
	return 0;
}
