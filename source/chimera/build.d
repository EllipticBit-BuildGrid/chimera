module chimera.build;

import std.array;
import std.algorithm.searching;
import std.file;
import std.path;
import std.process;
import std.stdio;
import std.string;

import sdlang;

import chimera.globals;
import chimera.utility;
import chimera.model.build;
import chimera.model.project;
import chimera.model.config;
import chimera.model.target;

public void build(string[] args) {
    //Parse command line arguments
    string[] profiles = string[].init;
    string[] configs = string[].init;
    string versionOverride = string.init;
    if (args.length > 0) {
        foreach(arg; args) {
            if (arg.startsWith("--profile=")) {
                profiles ~= arg[10..$];
            }
            else if (arg.startsWith("--profiles=")) {
                profiles ~= arg[11..$].split(";").array;
            }
            else if (arg.startsWith("--config=")) {
                configs ~= arg[9..$];
            }
            else if (arg.startsWith("--configs=")) {
                configs ~= arg[10..$].split(";").array;
            }
            else if (arg.startsWith("--version=")) {
                versionOverride = arg[10..$];
            }
            else {
                writeError("Unrecognized argument: " ~ arg);
                return;
            }
        }
    }

    //Get user configuration
    immutable UserConfiguration userConfig = getUserConfig();

    //Load project
    string projectPath = buildNormalizedPath(getcwd(), "chimera.project");    
    if (!exists(projectPath)) {
        writeError("Unable to locate project file in: " ~ getcwd());
        return;
    }
    Tag projectSdl = parseFile(projectPath);
    immutable Project project = new immutable Project(projectSdl, versionOverride);

    //Get the internal build definition
    immutable Build build = cast(immutable)getBuild(userConfig, project, dirName(projectPath), profiles, configs);
    
    //Check if we have errors before we continue
    abortOnError();

    foreach(c; build.configurations) {
        buildConfiguration(build, c);
    }
}

private void buildConfiguration(immutable Build build, immutable BuildConfiguration config) {
    writeln("Building Configuration: " ~ config.id);
    foreach(p; config.profiles) {
        buildProfile(build, config, p);
    }
}

private void buildProfile(immutable Build build, immutable BuildConfiguration config, immutable BuildProfile profile) {
    import std.conv;

    writeln("Building Profile: " ~ profile.id);

    if (!exists(profile.variables.intermediatePath)) {
        mkdirRecurse(profile.variables.intermediatePath);
    } else {
        safeRmDir(profile.variables.intermediatePath);
        mkdirRecurse(profile.variables.intermediatePath);
    }
    if (!exists(profile.variables.outputPath)) {
        mkdirRecurse(profile.variables.outputPath);
    } else {
        safeRmDir(profile.variables.outputPath);
        mkdirRecurse(profile.variables.outputPath);
    }
    
    immutable OutputSystem system = config.output.getSystem(config.platform, config.architecture);
    if (system is null) {
        writeError("Unable to locate target output for system: " ~ to!string(config.platform) ~ "+" ~ to!string(config.architecture));
        return;
    }

    foreach(t; system.Tools) {
        execute(t, build, config, profile, FlagInstance[].init);
    }
}

public void execute(
        immutable ToolInstance toolInstance,
        immutable Build build,
        immutable BuildConfiguration config,
        immutable BuildProfile profile,
        FlagInstance[] projectFlags) {
    immutable Tool tool = toolInstance.tool;
    bool useResponseFile = tool.ResponseFilePrefix != string.init;
    string flags = toolInstance.toFlags(profile, projectFlags, useResponseFile);
    string toolPath = getToolPath(cast(string[])tool.SearchPaths, cast(string)tool.Executable, tool.Version);
    string[] excludedDirs = string[].init;
    string searchDir = string.init;
    if (tool.inputTarget == ToolFileTarget.Source) {
        excludedDirs ~= profile.variables.intermediatePath;
        excludedDirs ~= profile.variables.outputPath;
        searchDir = profile.variables.sourcePath;
    } else if (tool.inputTarget == ToolFileTarget.Intermediate) {
        searchDir = profile.variables.intermediatePath;
    } else if (tool.inputTarget == ToolFileTarget.Output) {
        searchDir = profile.variables.outputPath;
    }

    string outputDir = string.init;
    if (tool.outputTarget == ToolFileTarget.Source) {
        outputDir = profile.variables.sourcePath;
    } else if (tool.outputTarget == ToolFileTarget.Intermediate) {
        outputDir = profile.variables.intermediatePath;
    } else if (tool.outputTarget == ToolFileTarget.Output) {
        outputDir = profile.variables.outputPath;
    }

    string[] inputFiles = getFiles(searchDir, cast(string[])tool.matchExtensions, excludedDirs);
    abortOnError();

    if (useResponseFile) {
        string responseFile = tool.ResponseFilePrefix ~ createResponseFile(toolInstance, profile.variables.intermediatePath, inputFiles, flags);
        auto pid = spawnShell(toolPath ~ " " ~ responseFile, stdin, stdout, stderr, null, Config.suppressConsole, outputDir);
        wait(pid);
    } else {
        string command = createCommandLine(toolInstance, toolPath, inputFiles, flags);
        auto pid = spawnShell(command, stdin, stdout, stderr, null, Config.suppressConsole, outputDir);
        wait(pid);
    }
}

private string toFlags(immutable ToolInstance toolInstance, immutable BuildProfile profile, FlagInstance[] projectFlags, bool useNewlineSeperator = false) {
    import std.ascii;

    immutable ToolInstance toolProfile = profile.tools[toolInstance.tool.Id];
    string prefix = toolInstance.tool.Flags.Prefix;
    string delim = toolInstance.tool.Flags.Delimiter;
    string sep = useNewlineSeperator ? newline : " ";
    string flags = string.init;

    //Write flags from the project file first.
    foreach(f; projectFlags) {
        flags ~= (cast(immutable)f).toString(prefix, delim, sep, toolInstance.tool.Flags, profile.variables);
    }
    //Write the profile flags next, ignoring flags already set by the project.
    foreach(f; toolProfile.flags) {
        if (canFind!((a, b) => a.flag == b)(projectFlags, f.flag)) continue;
        flags ~= f.toString(prefix, delim, sep, toolInstance.tool.Flags, profile.variables);
    }
    //Write the tool flags last, ignoring the project and profile flags.
    foreach(f; toolInstance.flags) {
        if (canFind!((a, b) => a.flag == b)(projectFlags, f.flag)) continue;
        if (canFind!((a, b) => a.flag == b)(toolProfile.flags, f.flag)) continue;
        flags ~= f.toString(prefix, delim, sep, toolInstance.tool.Flags, profile.variables);
    }

    return flags;
}

public string getToolPath(string[] searchPaths, string executable, immutable ToolVersion toolVersion) {
    foreach(path; searchPaths) {
        string toolPath = buildNormalizedPath(path, executable);
        if(exists(toolPath)) {
            if (toolVersion is null) {
                return toolPath;
            }
            else if (toolVersion.checkVersion(toolPath)) {
                return toolPath;
            } else {
                return null;
            }
        }
    }
    writeError("Unable to locate tool: " ~ executable ~ " Searched paths: " ~ searchPaths.join(":"));
    return null;
}

public string[] getFiles(string searchDirectory, string[] extensions, string[] excludeDirs) {
    DirEntry[] files = DirEntry[].init;
    foreach(ext; extensions) {
        files ~= dirEntries(searchDirectory, "*" ~ ext, SpanMode.depth).array;
    }

    string[] temp = string[].init;
    foreach (f; files) {
        bool isExcluded = false;
        foreach(ex; excludeDirs) {
            if (f.name.startsWith(ex)) {
                isExcluded = true;
            }
        }
        if (isExcluded || f.isDir) {
            continue;
        }
        temp ~= f.name;
    }

    if (temp.length == 0) {
        writeError("No files matching extensions: " ~ extensions.join(";"));
    }
    return temp;
}

private string createResponseFile(immutable ToolInstance toolInstance, string intermediatePath, string[] files, string flags) {
    import std.uuid;

    string fileName = buildNormalizedPath(intermediatePath, randomUUID().toString() ~ ".rsp");
    File responseFile = File(fileName, "w");
    scope(exit) {
        responseFile.flush();
        responseFile.close();
    }

    if (toolInstance.tool.FileListFirst) {
        foreach(f; files) {
            responseFile.writeln(f);
        }
        responseFile.writeln(flags);
    } else {
        responseFile.writeln(flags);
        foreach(f; files) {
            responseFile.writeln(f);
        }
    }

    return fileName;
}

private string createCommandLine(immutable ToolInstance toolInstance, string executablePath, string[] files, string flags) {
    string cmd = executablePath;

    if (toolInstance.tool.FileListFirst) {
        foreach(f; files) {
            cmd ~= f;
        }
        cmd ~= flags;
    } else {
        cmd ~= flags;
        foreach(f; files) {
            cmd ~= f;
        }
    }

    return cmd;
}
