module chimera.model.target;

import std.array;
import std.algorithm;
import std.conv;
import std.file;
import std.path;
import std.stdio;
import std.string;
public import std.typecons : BitFlags;
import std.typetuple;
import std.variant;

import sdlang;

import chimera.globals;
import chimera.model.build;

///
/// Flag Types
///

public enum FlagType
{
    Flag,
    Boolean,
    Integer,
    String,
    Values,
    Directory,
    File,
}

private FlagType getFlagType(string flagType) {
    if (flagType.toLower == "boolean") return FlagType.Boolean;
    if (flagType.toLower == "integer") return FlagType.Integer;
    if (flagType.toLower == "string") return FlagType.String;
    if (flagType.toLower == "values") return FlagType.Values;
    if (flagType.toLower == "directory") return FlagType.Directory;
    if (flagType.toLower == "file") return FlagType.File;
    return FlagType.Flag;
}

public alias FlagValueTypes = TypeTuple!(
	bool,
	int,
	string,
	typeof(null),
);
public alias FlagValue = Algebraic!(FlagValueTypes);

///
/// Flags
///

public immutable class FlagsList
{
    public immutable string Prefix;
    public immutable string Delimiter;
    public immutable Flag[string] Flags;

    public immutable this (Tag root) {
        this.Prefix = root.expectAttribute!string("prefix");
        this.Delimiter = root.expectAttribute!string("delimiter");

        Flag[string] tf;
        foreach(Tag t; root.all.tags) {
            immutable Flag nf = new immutable Flag(t);
            tf[nf.Id] = cast(Flag)nf;
        }
        tf.rehash();
        this.Flags = cast(immutable(Flag[string]))tf;
    }

    public bool hasFlag(string id) {
        return (id in Flags) !is null;
    }

    public bool verifyFlag(string id, string value, Location loc) {
        if (!hasFlag(id)) return false;
        immutable Flag f = Flags[id];

        if (f.Type == FlagType.String) {
            return true;
        }
        else if (f.Type == FlagType.Directory || f.Type == FlagType.File) {
            if (!isValidPath(value) || (f.MustExist && !exists(value))) {
                writeParseError("Unable to locate required path: " ~ value, loc);
                return false;
            }
        }
        else if (f.Type == FlagType.Values) {
            if(!f.Values.any!(a => a == value)()) {
                writeParseError("Unknown value '" ~ value ~ "' for flag: " ~ id, loc);
                return false;
            }
        } else {
            writeParseError("Incorrect data type for flag '" ~ id ~ "'. Expected a string.", loc);
            return false;
        }

        return true;
    }

    public bool verifyFlag(string id, long value, Location loc) {
        if (!hasFlag(id)) return false;
        immutable Flag f = Flags[id];

        if (f.Type == FlagType.Integer) {
            if (value < f.Min || value > f.Max) {
                writeParseError("The value " ~ to!string(value)  ~ " of flag '" ~ id ~ "' is outside of range " ~ to!string(f.Min) ~ "-" ~to!string(f.Max) , loc);
                return false;
            }
        } else {
            writeParseError("Incorrect data type for flag '" ~ id ~ "'. Expected an integer.", loc);
            return false;
        }

        return true;
    }

    public bool verifyFlag(string id, bool value, Location loc) {
        if (!hasFlag(id)) return false;
        immutable Flag f = Flags[id];

        if (f.Type != FlagType.Boolean) {
            writeParseError("Incorrect data type for flag '" ~ id ~ "'. Expected a boolean.", loc);
            return false;
        }

        return true;
    }
}

public immutable class Flag
{
    public immutable string Id;
    public immutable FlagType Type;
    public immutable Platforms Platform;
    public immutable Architectures Architecture;
    public immutable bool AllowMultiples;

    //Boolean type specific
    public immutable string TrueString;
    public immutable string FalseString;

    //Integer type specific
    public immutable long Min;
    public immutable long Max;

    //Directory+File type specific
    public immutable bool MustExist;
    public immutable bool ExpandSubDirectories;

    //Values type specific
    public immutable string[] Values;

    public immutable this(Tag root) {
        this.Id = root.name;
        this.Type = getFlagType(root.expectAttribute!string("type"));
        this.AllowMultiples = root.getAttribute!bool("allowMultiples", false);
        this.Platform = getPlatforms(root.getAttribute!string("platforms", "all"));
        this.Architecture = getArchitectures(root.getAttribute!string("architectures", "any"));

        //Boolean type specific
        if (this.Type == FlagType.Boolean) {
            this.TrueString = root.expectAttribute!string("trueString");
            this.FalseString = root.expectAttribute!string("falseString");
        } else {
            this.TrueString = null;
            this.FalseString = null;
        }

        //Integer type specific
        if (this.Type == FlagType.Integer) {
            this.Min = root.expectAttribute!int("min");
            this.Max = root.expectAttribute!int("max");
        } else {
            this.Min = long.min;
            this.Max = long.max;
        }

        //Directory+File type specific
        if (this.Type == FlagType.Directory || this.Type == FlagType.File) {
            this.MustExist = root.getAttribute!bool("mustExist", false);
        } else {
            this.MustExist = false;
        }
        if (this.Type == FlagType.Directory) {
            this.ExpandSubDirectories = root.getAttribute!bool("expandSubDirectories", false);
        } else {
            this.ExpandSubDirectories = false;
        }

        //Values type specific
        if (this.Type == FlagType.Values) {
            string[] temp = string[].init;
            foreach (Value v; root.values) {
                temp ~= v.get!string();
            }
            if (temp.length == 0) {
                writeParseError("Values Flag '" ~ this.Id ~ "' must have at least one value specified.", root.location);
            }
            this.Values = cast(immutable(string[]))temp;
        } else {
            this.Values = string[].init;
        }
    }
}

public immutable class FlagInstance 
{
    public immutable string flag;
    public immutable FlagValue value;

    public immutable this(string flag, FlagValue value) {
        this.flag = flag;
        this.value = value;
    }

    public immutable string toString(string prefix, string delim, string sep, immutable FlagsList definition, immutable BuildVariables variables) {
        if((flag in definition.Flags) is null) {
            writeError("Unable to locate flag: " ~ flag);
            return string.init;
        }

        immutable Flag flagdef = definition.Flags[flag];

        if (flagdef.Type == FlagType.Flag) {
            return prefix ~ this.flag ~ sep;
        } else {
            if (flagdef.Type == FlagType.Boolean) {
                bool flagval = this.value.get!bool();
                return prefix ~ this.flag ~ delim ~ (flagval ? flagdef.TrueString : flagdef.FalseString) ~ sep;
            }
            else if (flagdef.Type == FlagType.Directory) {
                string directory = this.value.get!string();
                string flags = prefix ~ this.flag ~ delim ~ "\"" ~ variables.expandVariables(directory) ~ "\"" ~ sep;
                if (!exists(directory) && flagdef.MustExist) {
                    return string.init;
                }
                if (flagdef.ExpandSubDirectories) {
                    DirEntry[] directories = dirEntries(directory, SpanMode.depth).filter!(a => a.isDir)().array;
                    foreach(d; directories) {
                        flags ~= prefix ~ this.flag ~ delim ~ "\"" ~ variables.expandVariables(d.name) ~ "\"" ~ sep;
                    }
                }
                return flags;
            }
            else if (flagdef.Type == FlagType.File) {
                string file = this.value.get!string();
                if (!exists(file) && flagdef.MustExist) {
                    return string.init;
                }
                return prefix ~ this.flag ~ delim ~ "\"" ~ variables.expandVariables(file) ~ "\"" ~ sep;
            }
            else if (flagdef.Type == FlagType.Integer) {
                int flagval = this.value.get!int();
                return prefix ~ this.flag ~ delim ~ to!string(flagval) ~ sep;
            }
            else  {
                string flagval = this.value.get!string();
                return prefix ~ this.flag ~ delim ~ variables.expandVariables(flagval) ~ sep;
            }
       }
    }
}

///
/// Tools
///

public enum ToolFileTarget
{
    None,
    Source,
    Intermediate,
    Output,
}
private ToolFileTarget getToolFileTarget(string fileTarget) {
    if (fileTarget.toLower == "source") return ToolFileTarget.Source;
    if (fileTarget.toLower == "intermediate") return ToolFileTarget.Intermediate;
    if (fileTarget.toLower == "output") return ToolFileTarget.Output;
    return ToolFileTarget.None;
}

public immutable class Tool
{
    public immutable string Id;
    public immutable string Executable;
    public immutable string[] SearchPaths;
    public immutable string[] matchExtensions;
    public immutable ToolFileTarget inputTarget;
    public immutable ToolFileTarget outputTarget;
    public immutable string ResponseFilePrefix;
    public immutable bool FileListFirst;
    public immutable ToolVersion Version;
    public immutable FlagsList Flags;

    public immutable this(Tag root) {
        this.Id = root.expectValue!string();
        this.Executable = root.expectTagValue!string("executable");
        this.ResponseFilePrefix = root.getTagValue!string("responseFilePrefix", null);
        this.FileListFirst = root.getTagValue!bool("fileListFirst", true);
        this.Flags = new immutable FlagsList(root.expectTag("flags"));

        Tag fhtag = root.getTag("fileHandling", null);
        if (fhtag !is null) {
            string fel = fhtag.getAttribute!string("matchExtensions", null);
            if (fel !is null) {
                string[] temp = fel.split(";");
                this.matchExtensions = cast(immutable(string[]))temp;
            } else {
                writeParseError("Expected attribute 'matchExtensions' on 'fileHandling'.", fhtag.location);
                this.matchExtensions = null;
            }
            this.inputTarget = getToolFileTarget(fhtag.expectAttribute!string("input"));
            this.outputTarget = getToolFileTarget(fhtag.expectAttribute!string("output"));
        } else {
            writeParseError("Expected tag 'fileHandling' not found.", root.location);
            this.matchExtensions = null;
            this.inputTarget = ToolFileTarget.None;
            this.outputTarget = ToolFileTarget.None;
        }

        Tag vtag = root.getTag("version", null);
        if (vtag !is null) {
            this.Version = new immutable ToolVersion(vtag);
        } else {
            this.Version = null;
        }

        Tag sptag = root.getTag("searchPaths", null);
        if (sptag !is null) {
            string[] temp = string[].init;
            foreach (Value v; sptag.values) {
                temp ~= v.get!string();
            }
            SearchPaths = cast(immutable)temp;
        } else {
            SearchPaths = string[].init;
        }
    }
}

public immutable class ToolVersion
{
    public immutable string Version;
    public immutable int Id;
    public immutable int CommandCompatibleId;
    public immutable int OutputCompatibleId;
    public immutable string CheckCmd;
    public immutable string CheckCmdVersion;

    public this(Tag root) {
        this.Version = root.expectValue!string();
        this.Id = root.expectAttribute!int("id");
        this.CommandCompatibleId = root.getAttribute!int("commandCompatibleId", 0);
        this.OutputCompatibleId = root.getAttribute!int("outputCompatibleId", 0);
        this.CheckCmd = root.getAttribute!string("checkCmd", null);
        this.CheckCmdVersion = root.getAttribute!string("checkCmdVersion", null);
    }

    public bool checkVersion(string path) {
        import std.process;

        if (this.CheckCmd is null || this.CheckCmdVersion is null) {
            return true;
        }

version(Posix) {
        string cmd = dirName(path) ~ "/" ~ this.CheckCmd;
}
version(Windows) {
        string cmd = dirName(path) ~ "\\" ~ this.CheckCmd;
}
        File stdout;
        auto result = executeShell(cmd);
        return canFind(result.output, this.CheckCmdVersion);
    }
}

public immutable class ToolInstance
{
    public immutable string id;
    public immutable FlagInstance[] flags;
    public immutable Tool tool;

    public immutable this(Tag root, immutable Tool tool) {
        this.id = root.name;
        this.tool = tool;

        FlagInstance[] tfv;
        parseFlags(root.values, tfv, tool, root.location);
        parseOptionFlags(root.all.attributes.array, tfv, tool);
        this.flags = cast(immutable(FlagInstance[]))tfv;

        this.verifyFlags();
    }

    private void verifyFlags() {
        foreach(fv; this.flags) {
            if (!canFind!((a, b) => a.flag == b)(this.flags, fv.flag)) {
                writeError("Unable to locate flag '" ~ fv.flag ~ "' for tool: " ~ this.id ~ " - This indicates a problem with the target file.");
                continue;
            }
            immutable Flag f = this.tool.Flags.Flags[fv.flag];
            if (f.Type == FlagType.Flag && fv.value != string.init) {
                writeError("Unepxected value '" ~ (cast(FlagValue)fv.value).toString() ~ "' found on flag '" ~ fv.flag ~ "' for tool: " ~ this.id ~ " - This indicates a problem with the target file.");
                continue;
            }
            else if (f.Type == FlagType.Boolean && fv.value.type != typeid(bool)) {
                writeError("Unepxected value '" ~ (cast(FlagValue)fv.value).toString() ~ "' found on flag '" ~ fv.flag ~ "' for tool: " ~ this.id ~ " - Expected: true/false - This indicates a problem with the target file.");
                continue;
            }
            else if (f.Type == FlagType.Integer) {
                if (fv.value.type != typeid(int)) {
                    writeError("Unepxected value '" ~ (cast(FlagValue)fv.value).toString() ~ "' found on flag '" ~ fv.flag ~ "' for tool: " ~ this.id ~ " - Expected: integer - This indicates a problem with the target file.");
                    continue;
                }
                if (fv.value.get!int() > f.Max || fv.value.get!int() < f.Min) {
                    writeError("Value '" ~ (cast(FlagValue)fv.value).toString() ~ "' out of range on flag '" ~ fv.flag ~ "' for tool: " ~ this.id ~ " - Expected: " ~ to!string(f.Min) ~ "-" ~ to!string(f.Max) ~ " - This indicates a problem with the target file.");
                    continue;
                }
            }
            else if (f.Type == FlagType.String) {
                if (fv.value.type != typeid(string)) {
                    writeError("Unepxected value '" ~ (cast(FlagValue)fv.value).toString() ~ "' found on flag '" ~ fv.flag ~ "' for tool: " ~ this.id ~ " - Expected: string - This indicates a problem with the target file.");
                    continue;
                }
            }
            else if (f.Type == FlagType.Values) {
                string values = string.init;
                foreach (v; f.Values) {
                    values ~= v ~ " ";
                }
                if (fv.value.type != typeid(string) || !f.Values.canFind(fv.value.get!string())) {
                    writeError("Unepxected value '" ~ (cast(FlagValue)fv.value).toString() ~ "' found on flag '" ~ fv.flag ~ "' for tool: " ~ this.id ~ " - Expected one of: " ~ values ~ "- This indicates a problem with the target file.");
                    continue;
                }
            }
            else if (f.Type == FlagType.File || f.Type == FlagType.Directory) {
                if (fv.value.type != typeid(string)) {
                    writeError("Unepxected value '" ~ (cast(FlagValue)fv.value).toString() ~ "' found on flag '" ~ fv.flag ~ "' for tool: " ~ this.id ~ " - Expected: string - This indicates a problem with the target file.");
                    continue;
                }
                string path = fv.value.get!string();
                if (f.MustExist && !exists(path)) {
                    writeError("Unable to locate directory '" ~ (cast(FlagValue)fv.value).toString() ~ "' specified with flag '" ~ fv.flag ~ "' for tool: " ~ this.id ~ " - This indicates a problem with the target file.");
                    continue;
                } else if (exists(path)) {
                    if (f.Type == FlagType.Directory && !isDir(path)) {
                        writeError("Path '" ~ (cast(FlagValue)fv.value).toString() ~ "' specified with flag '" ~ fv.flag ~ "' for tool '" ~ this.id ~ "' is not a directory - This indicates a problem with the target file.");
                        continue;
                    }
                    if (f.Type == FlagType.File && !isFile(path)) {
                        writeError("Path '" ~ (cast(FlagValue)fv.value).toString() ~ "' specified with flag '" ~ fv.flag ~ "' for tool '" ~ this.id ~ "' is not a file - This indicates a problem with the target file.");
                        continue;
                    }
                }
            }
        }
    }
}

private void parseFlags(Value[] values, ref FlagInstance[] flags, immutable Tool tool, Location loc) {
    foreach(Value v; values) {
        string flag = v.get!string();
        if (!tool.Flags.hasFlag(cast(immutable)flag)) {
            writeParseError("Unable to locate flag: " ~ flag, loc);
            continue;
        }
        flags ~= cast(FlagInstance) new immutable FlagInstance(flag, FlagValue(string.init));
    }
}

private void parseOptionFlags(Attribute[] attributes, ref FlagInstance[] flags, immutable Tool tool) {
    foreach(Attribute a; attributes) {
        string flag = a.name;
        if (flag.toLower() == "id") continue;
        if (!tool.Flags.hasFlag(cast(immutable)flag)) {
            writeParseError("Unable to locate flag: " ~ flag, a.location);
            continue;
        }

        if (a.value.type == typeid(int)) {
            int value = a.value.get!int();
            if (tool.Flags.verifyFlag(flag, value, a.location)) {
                flags ~= cast(FlagInstance)new immutable FlagInstance(flag, FlagValue(value));
            }
        }
        else if (a.value.type == typeid(string)) {
            string value = a.value.get!string();
            if (tool.Flags.verifyFlag(flag, value, a.location)) {
                flags ~= cast(FlagInstance)new immutable FlagInstance(flag, FlagValue(value));
            }
        }
        else if (a.value.type == typeid(bool)) {
            bool value = a.value.get!bool();
            if (tool.Flags.verifyFlag(flag, value, a.location)) {
                flags ~= cast(FlagInstance)new immutable FlagInstance(flag, FlagValue(value));
            }
        }
        else {
            writeParseError("Incorrect data type for flag '" ~ flag ~ "'. Expected a string/integer/boolean.", a.location);
            continue;
        }
    }
}

///
/// Profiles
///

public immutable class Profile
{
    public immutable string Id;
    public immutable ToolInstance[string] Tools;

    public immutable this(Tag root, Tool[string] tools) {
        this.Id = root.expectValue!string();

        ToolInstance[string] tl;
        foreach(Tag t; root.all.tags) {
            if ((t.name in tools) !is null) {
                ToolInstance tbpl = cast(ToolInstance)new immutable ToolInstance(t, cast(immutable)tools[t.name]);
                tl[tbpl.id] = tbpl;
            } else {
                writeParseError("Unable to locate tool '" ~ t.name ~ "' specified in profile: " ~ this.Id, t.location);
                continue;
            }
        }
        tl.rehash();
        this.Tools = cast(immutable(ToolInstance[string]))tl;
    }
}

///
/// Projects
///

public immutable class Output
{
    public immutable string Id;
    public immutable OutputSystem[string] Systems;
    public immutable bool allowAnyArch;

    public immutable this(Tag root, Tool[string] tools) {
        this.Id = root.expectValue!string();
        this.allowAnyArch = root.getAttribute!bool("allowAnyArchitecture", true);

        OutputSystem[string] tsl;
        foreach(Tag t; root.all.tags) {
            if (t.name.toLower() != "system") {
                writeParseError("Unrecognized tag '" ~ t.name ~ "' in project tag. Only 'system' tags are allowed inside a 'project' tag", t.location);
                continue;
            }
            OutputSystem tps = cast(OutputSystem)new immutable OutputSystem(t, tools, this.allowAnyArch);
            if ((tps.Id in tsl) is null) {
                tsl[tps.Id] = tps;
            } else {
                writeParseError("Project system '" ~ tps.Id ~ "' has already been defined and cannot be defined again.", t.location);
                continue;
            }
        }
        tsl.rehash();
        this.Systems = cast(immutable(OutputSystem[string]))tsl;
    }

    public immutable(OutputSystem) getSystem(Platform platform, Architecture arch) {
version(linux) {
        if (platform == Platform.All) {
            platform = Platform.Linux;
        }
}
version(OSX) {
        if (platform == Platform.All) {
            platform = Platform.MacOS;
        }
}
version(Windows) {
        if (platform == Platform.All) {
            platform = Platform.Windows;
        }
}
version(FreeBSD) {
        if (platform == Platform.All) {
            platform = Platform.FreeBSD;
        }
}
version(X86) {
        if (arch == Architecture.Any && !this.allowAnyArch) {
            arch = Architecture.X86;
        }
}
version(X86_64) {
        if (arch == Architecture.Any && !this.allowAnyArch) {
            arch = Architecture.X64;
        }
}
version(ARM) {
        if (arch == Architecture.Any && !this.allowAnyArch) {
            arch = Architecture.ARM;
        }
}
version(AArch64) {
        if (arch == Architecture.Any && !this.allowAnyArch) {
            arch = Architecture.ARM64;
        }
}
        string systemId = to!string(platform).toLower() ~ "+" ~ to!string(arch).toLower();
        return this.Systems[systemId];
    }
}

public immutable class OutputSystem
{
    public immutable string Id;
    public immutable Platform PlatformId;
    public immutable Architecture ArchitectureId;
    public immutable ToolInstance[] Tools;

    public immutable this(Tag root, Tool[string] tools, bool allowAnyArch) {
        this.PlatformId = getPlatform(root.expectAttribute!string("platform"));
        this.ArchitectureId = getArchitecture(root.expectAttribute!string("architecture"));
        this.Id = to!string(this.PlatformId).toLower() ~ "+" ~ to!string(this.ArchitectureId).toLower();
        if (!allowAnyArch && this.ArchitectureId == Architecture.Any) {
            writeParseError("This output system requires an architecture specification. 'Any' or 'null' are invalid.", root.location);
        }

        ToolInstance[] tl;
        foreach(Tag t; root.all.tags) {
            if ((t.name in tools) !is null) {
                tl ~= cast(ToolInstance)new immutable ToolInstance(t, cast(immutable)tools[t.name]);
            } else {
                writeParseError("Unable to locate tool '" ~ t.name ~ "' specified in profile: " ~ this.Id, t.location);
                continue;
            }
        }
        this.Tools = cast(immutable(ToolInstance[]))tl;
    }
}

///
/// Targets
///

public immutable class Target
{
    public immutable string Id;
    public immutable string ToolchainId;
    public immutable Platforms Platform;
    public immutable Architectures Architecture;

    public immutable Tool[string] Tools;
    public immutable Profile[string] Profiles;
    public immutable Output[string] Outputs;

    public immutable this(Tag root) {
        root = root.tags[0];
        this.Id = root.expectValue!string();
        this.ToolchainId = root.expectAttribute!string("toolchain");

        //Get supported platforms
        Tag pltag = root.expectTag("platforms");
        string[] tpl = string[].init;
        foreach (Value v; pltag.values) {
            tpl ~= v.get!string();
        }
        this.Platform = getPlatforms(tpl);

        //Get supported architectures
        Tag altag = root.expectTag("architectures");
        string[] tal = string[].init;
        foreach (Value v; altag.values) {
            tal ~= v.get!string();
        }
        this.Architecture = getArchitectures(tal);

        //Get Tools
        Tool[string] tt;
        foreach(Tag t; root.all.tags) {
            if (t.name.toLower() == "tool") {
                Tool nc = cast(Tool)new immutable Tool(t);
                tt[nc.Id] = nc;
            }
            //These are to ensure that we dont generate any spurious errors
            else if (t.name.toLower() == "output") continue;
            else if (t.name.toLower() == "profile") continue;
            else if (t.name.toLower() == "architectures") continue;
            else if (t.name.toLower() == "platforms") continue;
            else {
                writeParseError("Unrecognized target tag '" ~ t.name ~ "' specified.", t.location);
            }
        }
        tt.rehash();
        this.Tools = cast(immutable(Tool[string]))tt;

        //Get Profiles/Projects
        Profile[string] tp;
        Output[string] tproj;
        foreach(Tag t; root.all.tags) {
            if (t.name.toLower() == "profile") {
                Profile np = cast(Profile)new immutable Profile(t, tt);
                tp[np.Id] = np;
            }
            if (t.name.toLower() == "output") {
                Output np = cast(Output)new immutable Output(t, tt);
                tproj[np.Id] = np;
            }
        }
        tp.rehash();
        tproj.rehash();
        this.Profiles = cast(immutable(Profile[string]))tp;
        this.Outputs = cast(immutable(Output[string]))tproj;
    }
}
