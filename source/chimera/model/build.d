module chimera.model.build;

import std.algorithm;
import std.conv;
import std.file;
import std.path;
import std.stdio;

import chimera.model.project;
import chimera.model.target;
import chimera.model.config;
import chimera.globals;

public immutable class Build
{
    public immutable UserConfiguration userConfig;
    public immutable Meta metadata;
    public immutable BuildConfiguration[] configurations;

    public immutable this(
        immutable UserConfiguration userConfig,
        immutable Meta metadata,
        immutable BuildConfiguration[] configurations
    ) {
        this.userConfig = userConfig;
        this.metadata = metadata;
        this.configurations = configurations;
    }
}

public immutable class BuildConfiguration
{
    public immutable string id;
    public immutable Platform platform;
    public immutable Architecture architecture;
    public immutable Package[] packages;
    public immutable BuildFile[] files;

    public immutable Target target;
    public immutable Output output;
    public immutable BuildProfile[] profiles;

    public immutable this (
        string id,
        Platform platform,
        Architecture architecture,
        immutable Package[] packages,
        immutable BuildFile[] files,
        immutable Target target,
        immutable Output output,
        immutable BuildProfile[] profiles
    ) {
        this.id = id;
        this.platform = platform;
        this.architecture = architecture;
        this.packages = packages;
        this.files = files;
        this.target = target;
        this.output = output;
        this.profiles = profiles;
    }
}

public immutable class BuildFile
{
    public immutable string path;
    public immutable ConfigurationFileBuild buildAction;
    public immutable ConfigurationFileCopy copyMode;

    public immutable this(string path) {
        this.path = path;
        this.buildAction = ConfigurationFileBuild.Compile;
        this.copyMode = ConfigurationFileCopy.Never;
    }

    public immutable this(string path, immutable ConfigurationFile fileConfig) {
        this.path = path;
        this.buildAction = fileConfig.BuildAction;
        this.copyMode = fileConfig.CopyMode;
    }
}

public immutable class BuildProfile 
{
    public immutable string id;
    public immutable ToolInstance[string] tools;
    public immutable BuildVariables variables;

    public immutable this (immutable Profile profile, immutable Meta metadata, string sourcePath, string intermediatePath, string outputPath) {
        this.id = profile.Id;
        this.tools = profile.Tools;
        intermediatePath ~= "+" ~ this.id;
        outputPath ~= "+" ~ this.id;
        this.variables = new immutable BuildVariables(metadata, sourcePath, intermediatePath, outputPath);
    }
}

public immutable class BuildVariables
{
    public immutable string projectId;
    public immutable string projectTitle;
    public immutable string projectVersion;
    public immutable string buildTimestamp;
    public immutable string sourcePath;
    public immutable string intermediatePath;
    public immutable string outputPath;

    private immutable this (immutable Meta metadata, string sourcePath, string intermediatePath, string outputPath) {
        import std.datetime;

        this.projectId = metadata.Id;
        this.projectTitle = metadata.Title;
        this.projectVersion = metadata.Version;
        this.buildTimestamp = Clock.currTime().toUTC().toSimpleString();
        this.sourcePath = sourcePath;
        this.intermediatePath = intermediatePath;
        this.outputPath = outputPath;
    }

    public immutable string expandVariables(string input) {
        import std.string;

        string temp = input;
        if (input.canFind("{projectId}")) temp = temp.replace("{projectId}", this.projectId);
        if (input.canFind("{projectTitle}")) temp = temp.replace("{projectTitle}", this.projectTitle);
        if (input.canFind("{projectVersion}")) temp = temp.replace("{projectVersion}", this.projectVersion);
        if (input.canFind("{buildTimestamp}")) temp = temp.replace("{buildTimestamp}", this.buildTimestamp);
        if (input.canFind("{sourcePath}")) temp = temp.replace("{sourcePath}", this.sourcePath);
        if (input.canFind("{intermediatePath}")) temp = temp.replace("{intermediatePath}", this.intermediatePath);
        if (input.canFind("{outputPath}")) temp = temp.replace("{outputPath}", this.outputPath);

        return temp;
    }
}

public Build getBuild(immutable UserConfiguration userConfig, immutable Project project, string projectPath, string[] profiles = null, string[] configIds = null) {
    import std.algorithm.searching;

    //Get system targets
    auto targets = getSystemTargets();

    BuildConfiguration[] configurations;
    foreach(pc; project.Configurations.byKeyValue()) {
        if (!pc.value.Build) {
            continue;
        }
        if (configIds !is null && canFind(configIds, pc.key)) {
            configurations ~= getBuildConfigurations(cast(Configuration)pc.value, project.Metadata, targets, projectPath, profiles);
        } else if (configIds is null) {
            configurations ~= getBuildConfigurations(cast(Configuration)pc.value, project.Metadata, targets, projectPath, profiles);
        }
    }

    return cast(Build) new immutable Build(userConfig, project.Metadata, cast(immutable)configurations);
}

private BuildConfiguration[] getBuildConfigurations(Configuration config, immutable Meta metadata, immutable Target[string] targets, string projectPath, string[] profiles) {
    BuildConfiguration getBuildConfiguration(Platform platform, Architecture arch) {
        import std.array;
        import std.string;

        string bcid = config.Id ~ "+" ~ to!string(platform).toLower() ~ "+" ~ to!string(arch).toLower();
        immutable BuildFile[] bfl = cast(immutable)getBuildFiles(config, projectPath);

        string intermediatePath = buildNormalizedPath(projectPath, "obj", bcid);
        string outputPath = buildNormalizedPath(projectPath, "bin", bcid);
        
        //Get target
        Target target = null;
        if ((config.ToolchainId.toLower() in targets) is null) {
            writeError("Unable to locate target toolchain ID: " ~ config.ToolchainId);
        } else {
            target = cast(Target)targets[config.ToolchainId.toLower()];
        }

        //Get output
        if ((config.ProjectId.toLower() in target.Outputs) is null) {
            writeError("Unable to locate output: " ~ config.ProjectId);
            return null;
        }
        Output output = cast(Output)target.Outputs[config.ProjectId];

        //Get profiles
        BuildProfile[] pl = BuildProfile[].init;
        foreach(pid; config.ProfileIds) {
            if ((pid.toLower() in target.Profiles) is null) {
                writeError("Unable to locate profile: " ~ pid);
            } else if (canFind(profiles, pid) || profiles.length == 0) {
                pl ~= cast(BuildProfile) new immutable BuildProfile(target.Profiles[pid.toLower()], metadata, projectPath, intermediatePath, outputPath);
            }
        }

        return cast(BuildConfiguration) new immutable BuildConfiguration(bcid, platform, arch, cast(immutable)Package[].init, bfl, cast(immutable)target, cast(immutable)output, cast(immutable)pl);
    }

    BuildConfiguration[] bcl = BuildConfiguration[].init;
version (linux) 
{
    if ((config.PlatformType & Platform.Linux) || (!config.PlatformType)) {
        if (config.ArchitectureType & Architecture.X86) {
            bcl ~= getBuildConfiguration(Platform.Linux, Architecture.X86);
        }
        if (config.ArchitectureType & Architecture.X64) {
            bcl ~= getBuildConfiguration(Platform.Linux, Architecture.X64);
        }
        if (config.ArchitectureType & Architecture.ARM) {
            bcl ~= getBuildConfiguration(Platform.Linux, Architecture.ARM);
        }
        if (config.ArchitectureType & Architecture.ARM64) {
            bcl ~= getBuildConfiguration(Platform.Linux, Architecture.ARM64);
        }
        if (!config.ArchitectureType) {
            bcl ~= getBuildConfiguration(Platform.Linux, Architecture.Any);
        }
    }
}
version (OSX) 
{
    if ((config.PlatformType & Platform.MacOS) || (!config.PlatformType)) {
        if (config.ArchitectureType & Architecture.X86) {
            bcl ~= getBuildConfiguration(Platform.MacOS, Architecture.X86);
        }
        if (config.ArchitectureType & Architecture.X64) {
            bcl ~= getBuildConfiguration(Platform.MacOS, Architecture.X64);
        }
        if (config.ArchitectureType & Architecture.ARM) {
            bcl ~= getBuildConfiguration(Platform.MacOS, Architecture.ARM);
        }
        if (config.ArchitectureType & Architecture.ARM64) {
            bcl ~= getBuildConfiguration(Platform.MacOS, Architecture.ARM64);
        }
        if (!config.ArchitectureType) {
            bcl ~= getBuildConfiguration(Platform.MacOS, Architecture.Any);
        }
    }
}
version (Windows) 
{
    if ((config.PlatformType & Platform.Windows) || (!config.PlatformType)) {
        if (config.ArchitectureType & Architecture.X86) {
            bcl ~= getBuildConfiguration(Platform.Windows, Architecture.X86);
        }
        if (config.ArchitectureType & Architecture.X64) {
            bcl ~= getBuildConfiguration(Platform.Windows, Architecture.X64);
        }
        if (config.ArchitectureType & Architecture.ARM) {
            bcl ~= getBuildConfiguration(Platform.Windows, Architecture.ARM);
        }
        if (config.ArchitectureType & Architecture.ARM64) {
            bcl ~= getBuildConfiguration(Platform.Windows, Architecture.ARM64);
        }
        if (!config.ArchitectureType) {
            bcl ~= getBuildConfiguration(Platform.Windows, Architecture.Any);
        }
    }
}
version (FreeBSD) 
{
    if ((config.PlatformType & Platform.FreeBSD) || (!config.PlatformType)) {
        if (config.ArchitectureType & Architecture.X86) {
            bcl ~= getBuildConfiguration(Platform.FreeBSD, Architecture.X86);
        }
        if (config.ArchitectureType & Architecture.X64) {
            bcl ~= getBuildConfiguration(Platform.FreeBSD, Architecture.X64);
        }
        if (config.ArchitectureType & Architecture.ARM) {
            bcl ~= getBuildConfiguration(Platform.FreeBSD, Architecture.ARM);
        }
        if (config.ArchitectureType & Architecture.ARM64) {
            bcl ~= getBuildConfiguration(Platform.FreeBSD, Architecture.ARM64);
        }
        if (!config.ArchitectureType) {
            bcl ~= getBuildConfiguration(Platform.Windows, Architecture.Any);
        }
    }
}
    return bcl;
}

private BuildFile[] getBuildFiles(Configuration config, string rootPath) {
    BuildFile[] bfl;

    foreach(cf; config.Files) {
        string path = buildNormalizedPath(rootPath, cf.Path);
        if (exists(path)) {
            bfl ~= cast(BuildFile)new immutable BuildFile(path, cf);
        } else {
            writeError("Unable to locate file '" ~ cf.Path ~ "' at location: " ~ path);
        }
    }

    return bfl;
}
