module chimera.model.config;

import std.conv;
import std.stdio;
import std.string;

import sdlang;
import secured;

import chimera.globals;

public immutable class UserConfiguration
{
    public immutable string userName;
    public immutable string emailAddress;
    public immutable DependencySources sources;

    public immutable this(Tag root) {
        root = root.tags[0];
        this.userName = root.expectTagValue!string("userName");
        this.emailAddress = root.expectTagValue!string("emailAddress");

        Tag dtag = root.expectTag("dependencies");
        if (dtag !is null) {
            this.sources = new immutable DependencySources(dtag);
        } else {
            this.sources = null;
        }
    }

    public immutable this(string userName, string emailAddress, immutable DependencySources sources) {
        this.userName = userName;
        this.emailAddress = emailAddress;
        this.sources = sources;
    }

    public immutable Tag toTag() {
        auto root = new Tag(null, "configuration", null, [ new Attribute("version", Value(1)) ]);
        root.add(new Tag(null, "userName", [ Value (this.userName) ]));
        root.add(new Tag(null, "emailAddress", [ Value (this.emailAddress) ]));
        root.add(this.sources.toTag());
        return new Tag(null, null, null, null, [root]);
    }
}

public immutable class DependencySources
{
    public immutable string packageCache;
    public immutable DependencySource[string] sources;

    public immutable this(Tag root) {
        this.packageCache = root.expectTagValue!string("packageCache");

        Tag stag = root.expectTag("sources");
        DependencySource[string] tdsl;
        foreach(Tag t; stag.all.tags) {
            DependencySource tds = cast(DependencySource)new immutable DependencySource(t);
            if ((tds.id in tdsl) is null) {
                tdsl[tds.id] = tds;
            } else {
                writeParseError("Dependency source '" ~ tds.id ~ "' has already been defined and cannot be defined again.", t.location);
                continue;
            }
        }
        tdsl.rehash();
        this.sources = cast(immutable(DependencySource[string]))tdsl;
    }

    public immutable this(string packageCache, immutable DependencySource[string] sources) {
        this.packageCache = packageCache;
        this.sources = sources;
    }

    public immutable Tag toTag() {
        auto root = new Tag(null, "dependencies");
        root.add(new Tag(null, "packageCache", [ Value (this.packageCache) ]));
        auto sources = new Tag(null, "sources");
        foreach(ds; this.sources.byValue()){
            sources.add(ds.toTag());
        }
        root.add(sources);
        return root;
    }
}

public enum DependencySourceType
{
    Unknown,
    DUB,
    NuGet,
    BuildGrid
}
private DependencySourceType getDependencySourceType(string dst) {
    if (dst.toLower() == "dub") return DependencySourceType.DUB;
    if (dst.toLower() == "nuget") return DependencySourceType.NuGet;
    if (dst.toLower() == "buildgrid") return DependencySourceType.BuildGrid;
    return DependencySourceType.Unknown;
}

public immutable class DependencySource
{
    public immutable string id;
    public immutable DependencySourceType type;
    public immutable string url;
    public immutable string credentialUsername;
    public immutable string credentialPassword;
    public immutable string[] mirrors;

    public immutable this(Tag root) {
        this.type = getDependencySourceType(root.name);
        if (this.type == DependencySourceType.Unknown) {
            writeParseError("Invalid dependency source type: " ~ root.name, root.location);
            return;
        }

        this.id = root.expectValue!string();
        this.url = root.expectAttribute!string("url");

        Tag ctag = root.getTag("credentials", null);
        if (ctag !is null){
            this.credentialUsername = ctag.expectAttribute!string("username");
            this.credentialPassword = decryptPassword(ctag.expectAttribute!string("password"));
        } else {
            this.credentialUsername = null;
            this.credentialPassword = null;
        }

        Tag mtag = root.getTag("mirrors", null);
        if (mtag !is null) {
            string[] temp = string[].init;
            foreach (Value v; mtag.values) {
                temp ~= v.get!string();
            }
            this.mirrors = cast(immutable)temp;
        } else {
            this.mirrors = null;
        }
    }

    public immutable this(string id, DependencySourceType type, string url, string username = null, string password = null, string[] mirrors = null) {
        this.id = id;
        this.type = type;
        this.url = url;
        if (password !is null && password != string.init) {
            this.credentialUsername = username;
            this.credentialPassword = password;
        } else {
            this.credentialUsername = null;
            this.credentialPassword = null;
        }
        this.mirrors = cast(immutable(string[]))mirrors;
    }

    public immutable Tag toTag() {
        auto root = new Tag(null, to!string(this.type).toLower());
        root.add(Value(this.id));
        root.add(new Attribute("url", Value(this.url)));

        //Add credentials tag
        if (this.credentialUsername !is null) {
            Tag creds = new Tag(null, "credentials");
            creds.add(new Attribute("username", Value(this.credentialUsername)));
            creds.add(new Attribute("password", Value(encryptPassword(this.credentialPassword))));
            root.add(creds);
        }

        //Add mirrors tag
        if (this.mirrors !is null) {
            Value[] ml;
            foreach(string m; this.mirrors) {
                ml ~= Value(m);
            }
            root.add(new Tag(null, "mirrors", ml));
        }

        return root;
    }
}

public immutable(UserConfiguration) createUserConfig(string configPath) {
    import std.path;

    writeln("No user configuration found, creating new configuration.");
    write("Name: ");
    string name = readln();
    write("Email: ");
    string email = readln();
    writef("Package Cache Directory [Default: %s]: ", expandTilde("~/.buildgrid/packages"));
    string packageDir = readln();
    if (packageDir.strip() == string.init) {
        packageDir = expandTilde("~/.buildgrid/packages");
    }

    immutable dubSource = new immutable DependencySource("DUB Master Repo", DependencySourceType.DUB, "https://code.dlang.org/");
    immutable nugetSource = new immutable DependencySource("Nuget.org", DependencySourceType.NuGet, "https://api.nuget.org/v3/index.json");
    immutable DependencySource[string] sourcesArray = [ dubSource.id : dubSource, nugetSource.id : nugetSource ];
    immutable sources = new immutable DependencySources(packageDir, sourcesArray);
    immutable config = new immutable UserConfiguration(name, email, sources);

    Tag configSdl = config.toTag();
    auto configFile = File(configPath, "w");
    scope(exit) {
        configFile.flush();
        configFile.close();
    }
    configFile.write(configSdl.toSDLDocument());

    return config;
}

private string encryptPassword(string plainPwd) {
    if (configKey == null) {
        write("Please enter new configuration password: ");
        string pwd = readln();
        if (pwd.length < 8) {
            writeError("Passwords cannot be less than 8 characters.");
            return encryptPassword(plainPwd);
        }
        configKey = configWriteKey = hash(to!(ubyte[])(pwd));
    }
    else if (configWriteKey == null) {
        write("Please enter new configuration password or press enter to use current: ");
        string pwd = readln();
        if (pwd == string.init) {
            configWriteKey = configKey;
        } else {
            configKey = configWriteKey = hash(to!(ubyte[])(pwd));
        }
    }

    return to!string(encrypt(configWriteKey, to!(ubyte[])(plainPwd)));
}

private string decryptPassword(string encryptedPwd) {
    if (configKey == null) {
        write("Please enter configuration password: ");
        configKey = hash(to!(ubyte[])(readln()));
    }

    return to!string(decrypt(configKey, to!(ubyte[])(encryptedPwd)));
}


