module chimera.model.project;

import std.algorithm;
import std.conv;
import std.stdio;
import std.string;

import sdlang;

import chimera.globals;

///
/// Project
///

public immutable class Project
{
    public immutable Meta Metadata;
    public immutable Configuration[string] Configurations;

    public immutable this(Tag root, string versionOverride) {
        Tag mtag = root.getTag("metadata", null);
        if (mtag !is null) {
            this.Metadata = new immutable Meta(mtag, versionOverride);
        } else {
            writeParseError("Unable to locate 'metadata' tag in project.", root.location);
        }

        Configuration[string] cl;
        foreach(Tag t; root.all.tags) {
            if (t.name.toLower() == "configuration") {
                Configuration tc = cast(Configuration)new immutable Configuration(t);
                if ((tc.Id in cl) is null) {
                    cl[tc.Id] = tc;
                } else {
                    writeParseError("Configuration '" ~ tc.Id ~ "' is already specified.", t.location);
                    continue;
                }
            }
            else if (t.name == "metadata") continue;
            else {
                writeParseError("Unrecoginzed tag in '" ~ t.name ~ "' in project block.", t.location);
            }
        }
        this.Configurations = cast(immutable(Configuration[string]))cl;
    }

    public immutable this(immutable Meta metadata, immutable Configuration[string] configs) {
        this.Metadata = metadata;
        this.Configurations = configs;
    }

    public immutable Tag toTag() {
        auto root = new Tag(null, "project", null, [ new Attribute("version", Value(1)) ]);
        root.add(this.Metadata.toTag());

        foreach(immutable Configuration c; this.Configurations.byValue()){
            root.add(c.toTag());
        }

        return root;
    }
}

public immutable class Meta
{
    public immutable string Id;
    public immutable string Title;
    public immutable string Version;
    public immutable string[string] Authors;
    public immutable string[] Owners;
    public immutable string Copyright;
    public immutable string IconUrl;
    public immutable string ProjectUrl;
    public immutable string ReleaseNotesUrl;
    public immutable string SupportUrl;
    public immutable string LicenseUrl;
    public immutable bool RequireLicenseAcceptance;
    public immutable string Summary;
    public immutable string Description;
    public immutable string[] Tags;

    public immutable this (Tag root, string versionOverride)
    {
        this.Id = root.expectTagValue!string("id");
        this.Title = root.expectTagValue!string("title");
        if (versionOverride !is null && versionOverride.strip() != string.init) {
            this.Version = versionOverride;
        } else {
            this.Version = root.expectTagValue!string("version");
        }
        Tag al = root.getTag("authors", null);
        if (al !is null)
        {
            string[string] temp = string[string].init;
            foreach(Tag t; al.all.tags) {
                if (t.name.toLower() == "author") {
                    temp[t.expectAttribute!string("email")] = t.expectAttribute!string("name");
                } else {
                    writeParseError("Expected author tag, found: " ~ t.name, t.location);
                }
            }
            this.Authors = cast(immutable(string[string]))temp;
        } else {
            this.Authors = null;
        }
        string ol = root.getTagValue!string("owners", null);
        if (ol !is null)
        {
            string[] temp = string[].init;
            ol.split(";").each!((string s) => { if (s.strip() != "") temp ~= s.strip; });
            this.Owners = cast(immutable(string[]))temp;
        } else {
            this.Owners = null;
        }
        this.Copyright = root.getTagValue!string("copyright", null);
        this.IconUrl = root.getTagValue!string("iconUrl", null);
        this.ProjectUrl = root.getTagValue!string("projectUrl", null);
        this.ReleaseNotesUrl = root.getTagValue!string("releaseNotesUrl", null);
        this.SupportUrl = root.getTagValue!string("supportUrl", null);
        this.LicenseUrl = root.getTagValue!string("licenseUrl", null);
        this.RequireLicenseAcceptance = root.getTagValue!bool("requireLicenseAcceptance", false);
        this.Summary = root.getTagValue!string("summary", null);
        this.Description = root.getTagValue!string("description", null);
        string tl = root.getTagValue!string("tags", null);
        if (tl !is null)
        {
            string[] temp = string[].init;
            tl.split(";").each!((string s) => { if (s.strip() != "") temp ~= s.strip; });
            this.Tags = cast(immutable(string[]))temp;
        } else {
            this.Tags = null;
        }
    }

    public Tag toTag() {
        Tag root = new Tag(null, "metadata");

        root.add(new Tag(null, "id", [Value(this.Id)]));
        root.add(new Tag(null, "title", [Value(this.Title)]));
        root.add(new Tag(null, "version", [Value(this.Version)]));

        if (this.Authors.length > 0) {
            Tag[] tags;
            foreach(al; this.Authors.byKeyValue()) {
                tags ~= new Tag(null, "author", null, [ new Attribute("name", Value(al.value)), new Attribute("email", Value(al.key)) ], null);
            }
            root.add(new Tag(null, "authors", null, null, tags));
        }
        if (this.Owners.length > 0) {
            string str = string.init;
            foreach(string s; this.Owners) {
                str ~= s ~ ";";
            }
            root.add(new Tag(null, "owners", [Value(str)]));
        }
        if (this.Copyright !is null) root.add(new Tag(null, "copyright", [Value(this.Copyright)]));
        if (this.IconUrl !is null) root.add(new Tag(null, "iconUrl", [Value(this.IconUrl)]));
        if (this.ProjectUrl !is null) root.add(new Tag(null, "projectUrl", [Value(this.ProjectUrl)]));
        if (this.ReleaseNotesUrl !is null) root.add(new Tag(null, "releaseNotesUrl", [Value(this.ReleaseNotesUrl)]));
        if (this.SupportUrl !is null) root.add(new Tag(null, "supportUrl", [Value(this.SupportUrl)]));
        if (this.LicenseUrl !is null) root.add(new Tag(null, "licenseUrl", [Value(this.LicenseUrl)]));
        root.add(new Tag(null, "requireLicenseAcceptance", [Value(cast(bool)this.RequireLicenseAcceptance)]));
        if (this.Summary !is null) root.add(new Tag(null, "summary", [Value(this.Summary)]));
        if (this.Description !is null) root.add(new Tag(null, "description", [Value(this.Description)]));
        if (this.Tags.length > 0) {
            string str = string.init;
            foreach(string s; this.Tags) {
                str ~= s ~ ";";
            }
            root.add(new Tag(null, "tags", [Value(str)]));
        }

        return root;
    }
}

///
/// Packages
///

public enum PackageType
{
    Unknown,        // For unmatched package tags, always an error
    System,         // Packages installed on system
    Standard,       // Standard Libraries
    BuildGrid,      // BuildGrid packages
    NuGet,          // NuGet packages       -- .NET Only
    DUB,            // DUB packages         -- DLang Only
    GitHub,         // GitHub tags as packages
    Url,            // Download compressed files as packages
}
private PackageType getPackageType(string type) {
    if (type.toLower() == "system") return PackageType.System;
    if (type.toLower() == "standard") return PackageType.Standard;
    if (type.toLower() == "buildgrid") return PackageType.BuildGrid;
    if (type.toLower() == "nuget") return PackageType.NuGet;
    if (type.toLower() == "dub") return PackageType.DUB;
    if (type.toLower() == "github") return PackageType.GitHub;
    if (type.toLower() == "url") return PackageType.Url;
    return PackageType.Unknown;
}

public immutable class Package
{
    public immutable PackageType Type;
    public immutable string Id;
    public immutable string Version;
    public immutable string RepoId;
    public immutable string Url;
    public immutable string Configuration;
    public immutable Platforms PlatformType;
    public immutable Architectures ArchitectureType;

    public immutable this (Tag root) {
        this.Type = getPackageType(root.name);
        if (this.Type == PackageType.Unknown) {
            writeParseError("Unrecognized package type: " ~ root.name, root.location);
            return;
        }
        this.Id = root.expectAttribute!string("id");
        this.Version = root.expectAttribute!string("version");
        this.RepoId = root.getAttribute!string("repo", null);
        if (this.Type == PackageType.GitHub || this.Type == PackageType.DUB) {
            this.Url = root.expectAttribute!string("url");
        } else {
            this.Url = null;
        }
        this.Configuration = root.getAttribute!string("configuratios", null);
        this.PlatformType = getPlatforms(root.getAttribute!string("platforms", "all"));
        this.ArchitectureType = getArchitectures(root.getAttribute!string("architectures", "any"));
    }

    public immutable this(PackageType type, string id, string version_, string repoId, Platforms platform, Architectures architecture) {
        this.Type = type;
        this.Id = id;
        this.Version = version_;
        this.RepoId = repoId;
        this.PlatformType = platform;
        this.ArchitectureType = architecture;
    }

    public immutable Tag toTag() {
        auto root = new Tag(null, to!string(this.Type));
        root.add(new Attribute("id", Value(this.Id)));
        root.add(new Attribute("version", Value(this.Version)));
        if (this.RepoId !is null) {
            root.add(new Attribute("repo", Value(this.RepoId)));
        }
        return root;
    }
}

///
/// Configurations
///

public immutable class Configuration
{
    public immutable string Id;
    public immutable string ToolchainId;
    public immutable bool Build;
    public immutable Platforms PlatformType;
    public immutable Architectures ArchitectureType;
    public immutable string ProjectId;
    public immutable string[] ProfileIds;
    public immutable Package[] Dependencies;
    public immutable ConfigurationFile[] Files;

    public immutable this(Tag root) {
        this.Id = root.expectValue!string();
        this.ToolchainId = root.expectAttribute!string("toolchain");
        this.ProjectId = root.expectAttribute!string("project");
        this.PlatformType = getPlatforms(root.getAttribute!string("platforms", "all"));
        this.ArchitectureType = getArchitectures(root.getAttribute!string("architectures", "any"));
        this.Build = root.getAttribute!bool("build", true);

        //Parse profiles
        Tag ptag = root.getTag("profiles", null);
        if (ptag !is null)
        {
            string[] temp = string[].init;
            foreach(v; ptag.values) {
                if (v.type == typeid(string)) {
                    temp ~= v.get!string();
                } else {
                    writeParseError("Invalid profile data type for profile: " ~ this.Id, ptag.location);
                }
            }
            this.ProfileIds = cast(immutable(string[]))temp;
        } else {
            writeParseError("Must specify at least one profile for configuration: " ~ this.Id, root.location);
            this.ProfileIds = string[].init;
        }

        //Parse dependencies
        Tag dtag = root.getTag("dependencies", null);
        if (dtag !is null) {
            Package[] tpl;
            foreach(Tag t; dtag.all.tags) {
                tpl ~= cast(Package)new immutable Package(t);
            }
            this.Dependencies = cast(immutable(Package[]))tpl;
        }

        //Parse files
        ConfigurationFile[] fl;
        foreach(Tag t; root.all.tags) {
            if (t.name == "file") {
                fl ~= cast(ConfigurationFile)new immutable ConfigurationFile(t);
            }
            else if (t.name == "dependencies") continue;
            else if (t.name == "profiles") continue;
            else {
                writeParseError("Unrecoginzed tag in '" ~ t.name ~ "' in configuration block: " ~ this.Id, t.location);
            }
        }
        this.Files = cast(immutable(ConfigurationFile[]))fl;
    }

    public immutable Tag toTag() {
        auto root = new Tag(null, "configuration", [ Value(this.Id) ], [ new Attribute("toolchain", Value(this.ToolchainId)) ]);
        if (!this.Build) {
            root.add(new Attribute("build", Value(false)));
        }

        Value[] platforms = Value[].init;
        if (this.PlatformType & Platform.Linux) platforms ~= Value("linux");
        if (this.PlatformType & Platform.MacOS) platforms ~= Value("macos");
        if (this.PlatformType & Platform.Windows) platforms ~= Value("windows");
        if (this.PlatformType & Platform.FreeBSD) platforms ~= Value("freebsd");
        if (platforms.length > 0) {
            root.add(new Tag(null, "platforms", platforms));
        }

        Value[] archs = Value[].init;
        if (this.ArchitectureType & Architecture.X86) archs ~= Value("x86");
        if (this.ArchitectureType & Architecture.X64) archs ~= Value("x64");
        if (this.ArchitectureType & Architecture.ARM) archs ~= Value("arm");
        if (this.ArchitectureType & Architecture.ARM64) archs ~= Value("arm64");
        if (archs.length > 0) {
            root.add(new Tag(null, "architectures", archs));
        }

        //Write Profile IDs
        Value[] pids;
        foreach(string p; this.ProfileIds) {
            pids ~= Value(p);
        }
        root.add(new Tag(null, "profiles", pids));

        //Write dependencies
        Tag[] dl;
        foreach(immutable Package p; this.Dependencies) {
            dl ~= p.toTag();
        }
        root.add(new Tag(null, "dependencies", null, null, dl));

        //Write files configuration
        foreach(cf; this.Files) {
            root.add(cf.toTag());
        }

        return root;
    }
}

public enum ConfigurationFileBuild
{
    None,
    Content,
    Compile,
    Resource,
}
private ConfigurationFileBuild getConfigurationFileBuild(string build) {
    if (build.toLower() == "content") return ConfigurationFileBuild.Content;
    if (build.toLower() == "compile") return ConfigurationFileBuild.Compile;
    if (build.toLower() == "resource") return ConfigurationFileBuild.Resource;
    return ConfigurationFileBuild.None;
}

public enum ConfigurationFileCopy
{
    Never,
    Always,
    Newer,
}
private ConfigurationFileCopy getConfigurationFileCopy(string copy) {
    if (copy.toLower() == "never") return ConfigurationFileCopy.Never;
    if (copy.toLower() == "always") return ConfigurationFileCopy.Always;
    if (copy.toLower() == "newer") return ConfigurationFileCopy.Newer;
    return ConfigurationFileCopy.Never;
}

public immutable class ConfigurationFile
{
    public immutable string Path;
    public immutable ConfigurationFileBuild BuildAction;
    public immutable ConfigurationFileCopy CopyMode;

    public immutable this(Tag root) {
        this.Path = root.expectValue!string();
        this.BuildAction = getConfigurationFileBuild(root.getAttribute!string("buildAction", string.init));
        this.CopyMode = getConfigurationFileCopy(root.getAttribute!string("copyToOutput", string.init));

        if ((this.BuildAction == ConfigurationFileBuild.Compile ||
            this.BuildAction == ConfigurationFileBuild.Resource) &&
            this.CopyMode != ConfigurationFileCopy.Never) {
            writeParseError("Files marked as 'Compile' or 'Resource' cannot be copied to the output folder.", root.location);
        }
    }

    public immutable Tag toTag() {
        return new Tag(null, "file", [ Value(this.Path) ], [ new Attribute("buildAction", Value(to!string(this.BuildAction))), new Attribute("copyToOutput", Value(to!string(this.CopyMode))) ]);
    }
}
