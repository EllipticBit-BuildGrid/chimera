module chimera.tools.dub;

import std.process;

public void dubBuild(string packagePath, string configuration) {
    if (configuration !is null) {
        string buildCmd = "dub build --config=" ~ configuration;
        executeShell(buildCmd, Config.suppressConsole, null, size_t.max, packagePath);
    } else {
        string buildCmd = "dub build";
        executeShell(buildCmd, Config.suppressConsole, null, size_t.max, packagePath);
    }
}