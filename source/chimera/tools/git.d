module chimera.tools.git;

import std.path;
import std.process;

import chimera.model.config;
import chimera.model.project;

public void clone(string packagesPath, immutable Package gitPackage, immutable DependencySource[] source) {
    string packageId = gitPackage.Id ~ "." ~ gitPackage.Version;
    string clonedPath = buildNormalizedPath(packagesPath, packageId);
    string cloneCmd = "git clone " ~ gitPackage.Url ~ "\"" ~ packageId ~ "\"";
    string subInit = "git submodule init";
    string subUpdate = "git submodule update";

    executeShell(cloneCmd, Config.suppressConsole, null, size_t.max, packagesPath);
    executeShell(subInit, Config.suppressConsole, null, size_t.max, clonedPath);
    executeShell(subUpdate, Config.suppressConsole, null, size_t.max, clonedPath);
}

public void clean(string packagePath) {
    string cleanCmd = "git clean -fdx";

    executeShell(cleanCmd, Config.suppressConsole, null, size_t.max, packagePath);
}