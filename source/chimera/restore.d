module chimera.restore;

import std.string;

import chimera.globals;
import chimera.dependencies.dub;
import chimera.model.project;
import chimera.model.config;

public void restore(string[] args) {
    //Load user configuration
    immutable UserConfiguration userConfig = getUserConfig();

    //Load project
    string projectPath = buildNormalizedPath(getcwd(), "chimera.project");    
    if (!exists(projectPath)) {
        writeError("Unable to locate project file in: " ~ getcwd());
        return;
    }
    Tag projectSdl = parseFile(projectPath);
    immutable Project project = new immutable Project(projectSdl, versionOverride);

    abortOnError();

    restore(project, userConfig);
}

public void restore(immutable Project project, immutable UserConfiguration config) {
    Package[] restoreList = Package[].init;
    foreach (cfg; project.Configurations) {
        restoreList ~= cast(Package[])cfg.Dependencies;
    }
    restoreList = deduplicatePackages(restoreList);

    restoreDub(cast(immutable) restoreList);    
}

public Package[] deduplicatePackages(Package[] packages) {
    Package[string] tpl;

    foreach(pkg; packages) {
        string pid = pkg.Id.toLower() ~ pkg.Version.toLower();
        tpl[pid] = pkg;
    }

    return tpl.values;
}
