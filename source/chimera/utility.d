module chimera.utility;

import chimera.model.config;
import chimera.model.target;

// Safe directory deletion
public void safeRmDir(string path, bool recurse = true) {
    import core.thread;
    import std.file;

    rmdirRecurse(path);
    while (exists(path)) {
        Thread.sleep(100.msecs);
    }
}

public bool verifyGit() {
    import std.algorithm;
    import std.process;

    auto result = executeShell("git --version");
    return canFind("git version");
}

public immutable(UserConfiguration) getUserConfig() {
    import std.path;
    import std.file;
    import sdlang;

    string configPath = expandTilde("~/.buildgrid/chimera/user.config");
    UserConfiguration userConfig = null;

    if (!exists(expandTilde("~/.buildgrid")) || !exists(expandTilde("~/.buildgrid/chimera"))) {
        mkdirRecurse(expandTilde("~/.buildgrid/chimera"));
    }
    if (!exists(expandTilde("~/.buildgrid/packages"))) {
        mkdirRecurse(expandTilde("~/.buildgrid/packages"));
    }
    if (exists(configPath)) {
        Tag userConfigSdl = parseFile(configPath);
        return new immutable UserConfiguration(userConfigSdl);
    } else {
        return createUserConfig(configPath);
    }
}

public immutable(Target[string]) getSystemTargets() {
    import std.path;
    import std.file;
    import sdlang;

    auto targetsPath = buildNormalizedPath(dirName(thisExePath()), "targets");
    if (!exists(targetsPath)) {
        return null;
    }

    auto targetFiles = dirEntries(targetsPath, "*.target", SpanMode.depth);

    Tag[] targetRoots = Tag[].init;
    foreach(tf; targetFiles) {
        targetRoots ~= parseFile(tf.name);
    }

    Target[string] targets;
    foreach(t; targetRoots) {
        Target tgt = cast(Target)new immutable Target(t);
        targets[tgt.ToolchainId] = tgt;
    }

    targets.rehash;
    return cast(immutable(Target[string]))targets;
}
