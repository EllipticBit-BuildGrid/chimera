module chimera.help;

import std.stdio;

@safe:

public void writeHelp() {
    writeVersion();
}

public void writeVersion() {
    writeln("Chimera Build System 1.0.0");
    writeln("Copyright (C) 2018 by EllipticBit, LLC. All Rights Reserved.");
}
