module chimera.globals;

import core.atomic;
import std.algorithm;
import std.stdio;
import std.string;
import std.typecons;

import sdlang;

public shared uint errorCount = 0;
public shared uint deprecationCount = 0;
public __gshared ubyte[] configKey = null;
public __gshared ubyte[] configWriteKey = null;

public void writeParseError(string message, Location sdl) {
    atomicOp!"+="(errorCount, 1);
    writefln("%s(%d,%d) ERROR: %s", sdl.file, sdl.line, sdl.col, message);
}

public void writeError(string message) {
    atomicOp!"+="(errorCount, 1);
    writefln("ERROR: %s", message);
}

public void writeInfo(string message) {
    writefln("INFO: %s", message);
}

public void writeDeprecation(string message) {
    atomicOp!"+="(deprecationCount, 1);
    writefln("DEPRECATION: %s", message);
}

public void abortOnError() {
    import std.c.stdlib;

    uint errors = atomicLoad(errorCount);
    if (errors >= 1) {
        writefln("Encountered %d errors. Aborting.", errors);
        exit(1);
    }
}

///
/// Platform
///

public enum Platform : uint
{
    All,
    Linux = 1 << 0,
    MacOS = 1 << 1,
    Windows = 1 << 2,
    FreeBSD = 1 << 3,
}
public alias Platforms = BitFlags!Platform;

public Platform getPlatform(string platform) {
    if (platform.toLower == "linux") return Platform.Linux;
    if (platform.toLower == "macos") return Platform.MacOS;
    if (platform.toLower == "windows") return Platform.Windows;
    if (platform.toLower == "freebsd") return Platform.FreeBSD;
    return Platform.All;
}

public Platforms getPlatforms(string[] platforms) {
    Platforms flags;

    foreach(string p; platforms) {
        if (p.toLower() == "linux") flags |= Platform.Linux;
        if (p.toLower() == "macos") flags |= Platform.MacOS;
        if (p.toLower() == "windows") flags |= Platform.Windows;
        if (p.toLower() == "freebsd") flags |= Platform.FreeBSD;
    }

    return flags;
}

public Platforms getPlatforms(string platforms) {
    Platforms flags;

    string[]pl = string[].init;
    platforms.split(";").each!((string s) => pl ~= s.toLower());
    foreach(string p; pl) {
        if (p.toLower() == "linux") flags |= Platform.Linux;
        if (p.toLower() == "macos") flags |= Platform.MacOS;
        if (p.toLower() == "windows") flags |= Platform.Windows;
        if (p.toLower() == "freebsd") flags |= Platform.FreeBSD;
    }

    return flags;
}

///
/// Architecture
///

public enum Architecture : uint
{
    Any,
    X86 = 1 << 0,
    X64 = 1 << 1,
    ARM = 1 << 2,
    ARM64 = 1 << 3,
}
public alias Architectures = BitFlags!Architecture;

public Architecture getArchitecture(string arch) {
    if (arch.toLower == "x86") return Architecture.X86;
    if (arch.toLower == "x64") return Architecture.X64;
    if (arch.toLower == "arm") return Architecture.ARM;
    if (arch.toLower == "arm64") return Architecture.ARM64;
    return Architecture.Any;
}

public Architectures getArchitectures(string[] archs) {
    Architectures flags;

    foreach(string a; archs) {
        if (a.toLower() == "x86") flags |= Architecture.X86;
        if (a.toLower() == "x64") flags |= Architecture.X64;
        if (a.toLower() == "arm") flags |= Architecture.ARM;
        if (a.toLower() == "arm64") flags |= Architecture.ARM64;
    }

    return flags;
}

public Architectures getArchitectures(string archs) {
    Architectures flags;

    string[]al = string[].init;
    archs.split(";").each!((string s) => al ~= s.toLower());
    foreach(string a; al) {
        if (a.toLower() == "x86") flags |= Architecture.X86;
        if (a.toLower() == "x64") flags |= Architecture.X64;
        if (a.toLower() == "arm") flags |= Architecture.ARM;
        if (a.toLower() == "arm64") flags |= Architecture.ARM64;
    }

    return flags;
}
