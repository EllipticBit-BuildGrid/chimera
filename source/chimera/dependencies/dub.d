module chimera.dependencies.dub;

import std.algorithm.searching;
import std.array;
import std.path;
import std.string;
import std.stdio;

import vibe.data.json;
import vibe.web.rest;

import chimera.globals;
import chimera.utility;
import chimera.model.config;
import chimera.model.project;

struct SearchResult
{
    string id;
    string description;
    string versionId;
}

interface DubRegistry
{
	@method(HTTPMethod.GET)
	SearchResult[] search(string q = "");

	@path(":name/latest")
	string getLatestVersion(string _name);

	@path(":name/info")
	Json getInfo(string _name);

	@path(":name/:versionId/info")
	Json getInfo(string _name, string _versionId);
}

private DubRegistry createDubPackageInterface(string url) {
    return new RestInterfaceClient!DubRegistry(url);
}

public void restoreDub(immutable Package[] packages, string packagePath) {
    if (!packages.any!(a => a.Type == PackageType.DUB)()) {
        return;
    }
    if (!verifyGit()) {
        writeError("Unable to locate suitable git client.");
        return;
    }

    packagePath = buildNormalizedPath(packagePath, "dub");
    immutable UserConfiguration userConfig = getUserConfig();

    foreach (pkg; packages) {
        if (pkg.Type != PackageType.DUB) {
            continue;
        }

        if (pkg.RepoId !is null && ((pkg.RepoId in userConfig.sources.sources) !is null)) {
            auto pkgSource = userConfig.sources.sources[pkg.RepoId];
            DubRegistry[] registries = [ createDubPackageInterface(pkgSource.url) ];
            retrieveDubPackage(pkg.Id, pkg.Version, registries);
        } else {
            DubRegistry[] registries = DubRegistry[].init;
            foreach(src; userConfig.sources.sources.byValue()) {
                registries ~= createDubPackageInterface(src.url);
            }
            retrieveDubPackage(pkg.Id, pkg.Version, registries);
        }
    }
}

private bool retrievePackage(string name, string versionId, DubRegistry[] registries) {
    foreach(reg; registries) {
        Json result = reg.getInfo(name, versionId);
        writeln(result.toString());
        return true;
    }

    return false;
}
